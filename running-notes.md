# Running note for our project
2022-06-27

Friday: 10-12 minutes presentation. Show the code, but more interesting is summary of group interaction. What worked, what did not.

# Ali's basic requirements
- Not interested in implementing every feature of the project. Rather, we should experience everything we've seen the last week(s). There has to be a minimal working example.
- conversational development
- repo with multiple commits
- continuous integration: with automatic unit testing
- If you have never done f.ex. the tests before, then you should volunteer to do that. Even if you fail at doing the tests, you experienced doing it.
- If you want to implement a feature that is not part of the minimal requirement, consult Ali **first**.

# To do
- Define the scope
- Set the requirements
- Define the expected outcome
- Structure work and execute (time estimations)
- Assign roles and responsibilities
- Test and validate
- Document
- At the end of the day we present to Ali

## Define the scope
- Something presentable: a minimum functioning result.
- Project management
- Graph with multiple plots based on octamer values with interactivity.

# Requirements
## Must have
- Load FASTA files (As strings or a dictionary)
- Load txt files. (Pandas)
- Scoring function (Open reading frame for octamer value)
- Create plots
- Interactive with plots
- Unit tests
- Follow clean code rules, PEP8.
- Conda requirement

## Nice to have
- configurable code (able to add new file with different files)
- automatic documentation. Sphinx will do this.
- makefile that runs it all. If it is possible to use it.

# Work and time estimations
- Load FASTA files 
    - input: fasta 
    - output: (As strings in dataframe) pandas
- Load txt files. 
    - input: txt
    - output: a Pandas dataframe
- Scoring function (Open reading frame for the string in fasta file, and moves by eight bases and comapres to octamers)
    - input: the fasta and octamer objects
    - output: pandas(position, sequence 1 letter (wildtype), sequence 1 value, seq2 value, seq3 value, seq4 value)
- Create plots
    - input: the two pandas dataframes
    - output: plot object
- Interactive with plots
    - input: the create plots
    - user interface
- Unit testing

# Work roles and time estimation
- Agata: 
    - Scoring: finished Wednesday (4h)
- Amadine: Plotting:  
    - minimal plot finished Tues (3h)
    - finished plot (3+3h) Wed
- Ammy: Loading: finished Tues (1h)
- Håvard: Group leader, documentation, 
    - testing (6h)
- Thurs: 
    - continuous integration (2h)
    - testing, 
    - finishing documentation (2h), 
    - presentation practice (0.5h)
