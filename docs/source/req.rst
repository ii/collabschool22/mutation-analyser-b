.. _mainreq:

Requirements
++++++++++++


* Matplotlib
* Numpy
* Pandas
* Sphinx


.. figure:: ../output.PNG