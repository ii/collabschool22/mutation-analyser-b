# -*- coding: utf-8 -*-
"""
Created on Mon Jun 27 18:09:27 2022

@author: Agata and Amrinder
"""

import pandas as pd


def seq_loader(file_dir):
    """
    Generates and returns a random number.
    Reads fasta file and return dictionary. 
    Input in form of fasta file.
    
    Returns
    -------
    dict
        Dictionary {'sequence_name':['sequence']}
    """
    fasta_seq = {}
    with open(file_dir) as file_one:
        for line in file_one:
            line = line.strip()
            if not line:
                continue
            if line.startswith(">"):
                active_sequence_name = line[1:]
                if active_sequence_name not in fasta_seq:
                    fasta_seq[active_sequence_name] = []
            else:
                sequence = line
                fasta_seq[active_sequence_name] = sequence

    return fasta_seq


def octamer_loader(file_dir_oct):
    """
    Reads tet file (csv) and stores the values in a dictionary.

    Returns
    -------
    dict
        Dictionary {'octamer_sequence':[P-index,I-index]}
    """
    octamer_seq = pd.read_csv(file_dir_oct,
                              header=0,
                              skiprows=(22),
                              sep=",",
                              index_col=False)

    octamer_seq = octamer_seq.drop('Unnamed: 3', axis=1)

    octamer_dict = octamer_seq.set_index('Octamer').T.to_dict('list')

    return octamer_dict

if __name__ == "__main__":
    file_dir = r"..\data\test.fasta"
    sequences = seq_loader(file_dir)

    file_dir_oct = r"..\data\octamers.txt"
    octamers = octamer_loader(file_dir_oct)
