# -*- coding: utf-8 -*-
"""
Created on Wed Jun 29 14:55:01 2022

@author: asi073
"""


"""
Program to compare the nucleotide sequencing and detect mutation

"""

import pandas as pd # importing pandas library for data handling 
import numpy as np  # importing numpy library for numerical calculation


def compare_strings(seq1, seq2):
    """
    Function to comparing two nucleotide sequences

    """

    seq_list = [] # create a emply list
    for index, cumsum in enumerate(zip(seq1, seq2)): #adds a counter to an iterable and returns it in a form of enumerating object
        tup_a, tup_b = cumsum
        if tup_a != tup_b:
            seq_list.append([index, tup_a, tup_b])
    return seq_list

    
def detect_mutation(sequence_values):
    """
    Function to detect mutaiton in the sequneces compared with 'wt' sequence

    """

    df_sequences = pd.DataFrame(data={'WT': list(sequence_values['wt'])})
    
    # creating a list to store string of sequences
    sequence_names = ["wt", "M30", "M7", "M11x", "M11"] 
    
    # a list to store comparison per base
    for seq_name in sequence_names:
        temp_list = compare_strings(sequence_values[sequence_names[0]], 
                                    sequence_values[seq_name])
        # check if only one mutation was found
        if np.shape(temp_list)[0] == 1: 
            df_sequences.at[temp_list[0][0], seq_name] = temp_list[0][2].upper()
        else:
            for mutation_index in range(np.shape(temp_list)[0]):
                df_sequences.at[temp_list[mutation_index][0], seq_name] = temp_list[mutation_index][2].upper()

    return df_sequences