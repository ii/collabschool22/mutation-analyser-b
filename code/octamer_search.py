# -*- coding: utf-8 -*-
"""
Created on Wed Jun 29 11:03:34 2022

@author: Amandine & Agata

Octamer matching between sliced sequence and octamers dictionary. 
Function returns pandas dataframe with P- and I-indices listed within the 
respective cells. 
"""
import numpy as np
import pandas as pd

def move_reading_frame(sequence_values):
    subsequences = np.empty([len(list(sequence_values.values())[0])-6, len(sequence_values.keys())], dtype='U25')

    for seq_name_index, sequence_name in enumerate(sequence_values.keys()):
        current_sequence = sequence_values[sequence_name]
        subsequences[0, seq_name_index] = sequence_name
        for index, _ in enumerate(current_sequence):
            subsequences[index + 1, seq_name_index] = current_sequence[index:index + 8].lower()
            if index == len(current_sequence) - 8:
                break

    return subsequences


def compare_subseq_to_octamers(sub_seq_values, octamer_values):
    sub_seq_df = pd.DataFrame(sub_seq_values, columns=sub_seq_values[0,:])
    sub_seq_df = sub_seq_df.drop(0)

    def map_helper(seq):
        return octamer_values[seq]

    return sub_seq_df.applymap(map_helper)


