# -*- coding: utf-8 -*-
"""
Created on Tue Jun 28 16:53:20 2022

@author: havaka

This is the main script, that calls other functions and 
data, and puts it all together into a beautfiful mess.

"""
from loader_function import seq_loader, octamer_loader
from plot import create_basic_plot, create_interactive_plot
from detectMut import detect_mutation
from octamer_search import move_reading_frame, compare_subseq_to_octamers
from save_score import save_score

import pandas as pd
import numpy as np


file_dir = r"..\data\test.fasta"
sequences = seq_loader(file_dir)

file_dir_oct= r"..\data\octamers.txt" 
octamers = octamer_loader(file_dir_oct)

sub_sequences = move_reading_frame(sequences)

octama_indices = compare_subseq_to_octamers(sub_sequences, octamers)


df_sequences = detect_mutation(sequences)

sub_sequences = move_reading_frame(sequences)

octama_indices = compare_subseq_to_octamers(sub_sequences, octamers)
    
   
scores = save_score(octama_indices)

# Add empty rows to score dataframe to match it up with the sequence
data = []
data.insert(0, {'wt': np.nan, 'M30': np.nan, 'M7': np.nan, 'M11x': np.nan, 'M11': np.nan})
data.insert(0, {'wt': np.nan, 'M30': np.nan, 'M7': np.nan, 'M11x': np.nan, 'M11': np.nan})
data.insert(0, {'wt': np.nan, 'M30': np.nan, 'M7': np.nan, 'M11x': np.nan, 'M11': np.nan})
data.insert(0, {'wt': np.nan, 'M30': np.nan, 'M7': np.nan, 'M11x': np.nan, 'M11': np.nan})

scores = pd.concat([pd.DataFrame(data), scores, pd.DataFrame(data)], ignore_index=True)


#create_basic_plot(df_sequences, score)

create_interactive_plot(df_sequences, scores, octamers)

