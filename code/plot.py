import matplotlib.pyplot as plt
import numpy as np

from octamer_search import move_reading_frame, compare_subseq_to_octamers
from save_score import save_score

# Define global internal variables
_x_coordinate_user_input = 0
_y_coordinate_user_input = 0
_key_user_input = 'X'


def add_sequence_text_to_plot(axes, sequences_values, score_values):
    # Convert all values in wt column to upper case
    sequences_values['WT'] = sequences_values['WT'].str.upper()

    # Define boxes for sequence text
    props = {'facecolor': 'white', 'edgecolor': 'none', 'pad': 0}

    # Add sequences to plot
    # Define height of text for mutation value
    text_height = int(score_values.min().min())
    for column in sequences_values.columns:
        if column == 'WT':
            for idx, value in sequences_values[column].iteritems():
                # Add value to plot
                axes.text(idx, text_height, value, bbox=props, fontsize=11, ha='center', va='center', color='black')
            # Add label
            axes.text(125, text_height, column, fontsize=12.3, color='black')
            text_height += 1
        else:
            # Find all mutations in given column
            mutations = sequences_values[column][sequences_values[column].notna()]
            for idx, value in mutations.iteritems():
                # Plot vertical lines at every mutation
                axes.vlines(idx, linestyle='--', ymin=score_values.min().min(), ymax=score_values.max().max(),
                            color='black', zorder=15)
                # Add mutation value to plot
                axes.text(idx, text_height, value, bbox=props, fontsize=11, ha='center', va='center', color='black',
                          zorder=20)
            # Add label for mutation
            axes.text(125, text_height, column, fontsize=12.3, color='black')

            text_height += 1


def create_basic_plot(sequences, scores, ax):
    # Clear axes to allow updating of plot after user input
    ax.clear()

    # Set font size
    plt.rc('font', **{'size': '13'})

    # Remove tick labels on x axes
    ax.set_xticklabels('')

    # Add score values
    scores.plot(ax=ax, zorder=10, lw=2)
    # Add zero line
    ax.plot(scores.index, np.repeat(0, len(scores.index)), linestyle='--', color='black', zorder=5)
    # Add non-functional band
    ax.fill_between(x=scores.index, y1=-2.6, y2=2.6, alpha=0.2, facecolor='yellow', label='non-functional segments',
                    zorder=0)
    # Add text for sequences
    add_sequence_text_to_plot(ax, sequences, scores)

    # Add axes labels to figure
    ax.set_ylabel('Chasin z-score')
    ax.set_xlabel('index')

    # Add legend
    plt.legend()


def get_click_coordinates(event):
    global _x_coordinate_user_input
    global _y_coordinate_user_input
    # Get x and y coordinates of where user clicked
    _x_coordinate_user_input = int(np.round(event.xdata))
    _y_coordinate_user_input = int(event.ydata)
    print("You will add a mutation at : (" + str(_x_coordinate_user_input) + ", " + str(_y_coordinate_user_input) + ")")


def handle_user_input(event, current_sequences, all_octamers, ax):
    global _key_user_input
    # Get input from users keyboard
    _key_user_input = event.key.upper()

    if check_user_input_is_valid(_key_user_input):
        # Update existing sequence dataframe with user input/ mutation
        # Note: only column M30 can be updated
        current_sequences = update_sequence_with_user_input(current_sequences, _x_coordinate_user_input,
                                                            _key_user_input)
        # To recalculate the score the sequence dataframe is turned into a dictionary
        current_sequences_dict = turn_seq_dataframe_in_dict(current_sequences.copy())

        sub_sequences = move_reading_frame(current_sequences_dict)
        octama_indices = compare_subseq_to_octamers(sub_sequences, all_octamers)
        updated_score = save_score(octama_indices)

        # Update plot with user input
        create_basic_plot(current_sequences, updated_score, ax)
        plt.gcf().canvas.draw_idle()


def check_user_input_is_valid(value):
    if value not in ['A', 'C', 'T', 'G']:
        print("You have typed: " + str(value) + ". This is not a valid bases. Please enter a knew one.")
        return False
    else:
        print("You have added the bases: " + str(value) + " to the sequence M30.")
        return True


def update_sequence_with_user_input(cur_sequences, x_coord, bases):
    cur_sequences.at[x_coord, 'M30'] = bases
    return cur_sequences


def turn_seq_dataframe_in_dict(seq):
    # Get all columns which are not wt
    columns = seq.columns.difference(['WT'])
    # Fill all rows which are empty with values from wt column
    for col in columns:
        seq[col].loc[seq[col].isna()] = seq['WT'].loc[seq[col].isna()]
    # Turn dataframe into dictionary
    seq_dict = seq.to_dict('list')
    # Turn dictionary values into one continuous string
    for key in seq_dict.keys():
        seq_dict[key] = ''.join(map(str, seq_dict[key]))

    return seq_dict


def create_interactive_plot(sequences, scores, octamers):
    # Create figure
    figure, axes = plt.subplots()
    # Create first basic plot
    create_basic_plot(sequences, scores, axes)
    # Handle click on figure and user input
    figure.canvas.mpl_connect('button_press_event', get_click_coordinates)
    figure.canvas.mpl_connect('key_press_event', lambda event: handle_user_input(event, sequences, octamers, axes))

    plt.show()
