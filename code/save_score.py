"""
Function created on 30.06.2022 15:30 by ATW
 
Function allows to recover indices values from pandas DataFrame cells and 
returns the enhancer/silencer affinity score for octamers in given fasta sequences
"""
import pandas as pd

def save_score(octama_indices):
    """applies helper function and returns the enhancer/silencer affinity score"""
    score_array = octama_indices.applymap(calculate_score)
    return(score_array)
    
def calculate_score(cell):
    """cells from octama_indices are subjected to score calculations"""
    treshold = 2.6
    Pindex, Iindex = cell
    t_upp = treshold
    t_low = -treshold
    if t_low > Pindex and t_low > Iindex:
        score = max(Pindex, Iindex)
    elif t_upp < Pindex and t_upp < Iindex :
        score = min(Pindex, Iindex)
    else:
        score = (Pindex + Iindex)/2
    return(score)  
