# -*- coding: utf-8 -*-
"""
Created on Tue Jun 28 16:03:02 2022

@author: havaka
"""
# Import function to be tested.

from loader_function import seq_loader



def test_seq_loader():
    """
    Checks that the elements of the list of fasta objects
    are not incorrectly entered as a letter other than 
    A, C, G, or T.
    """
    sequences = seq_loader("tests/test.fasta")

    letter_list = (["B", "D", "E", "F", "H", "I", "J", "K", 
                    "L", "M", "N", "O", "P", "Q", "R", "S", 
                    "U", "V", "W", "X", "Y", "Z"])
    
    letter_list_in_seq = False
    for value in sequences.values():
        for letter in letter_list:
            if letter in value.upper():
                letter_list_in_seq=True            
    assert (letter_list_in_seq == False)

def test_seq_loader_fail():
    """
    Checks that the elements of the list of fasta objects
    are not incorrectly entered as a letter other than 
    A, C, G, or T.
    """
    sequences = seq_loader("tests/test_fail.fasta")

    letter_list = (["B", "D", "E", "F", "H", "I", "J", "K", 
                    "L", "M", "N", "O", "P", "Q", "R", "S", 
                    "U", "V", "W", "X", "Y", "Z"])
    for value in sequences.values():
        for letter in letter_list:
            if letter in value.upper():
                letter_list_in_seq=True            
    assert (letter_list_in_seq == True)
    
    
    
    