# -*- coding: utf-8 -*-

""" utils.py : Contains some utilities """



from os import path

def validate(F, O):
    type_F = path.splitext(F)
    type_O = path.splitext(O)
    iscompatible = False
    if type_F == '.fasta':
        iscompatible = True
        casetext = 'is fasta file'
    elif type_O == '.txt':
        iscompatible = True
        casetext = 'is octamer file'
    return iscompatible, type_F, type_O, casetext